### common code

import os
import csv

def ReadCSVFile(fileName, splitStr=",", dbg=False):

    print('ReadCSVFile: to file read ("'+splitStr+'"): '+fileName)

    objects=[]
    with open(fileName, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=splitStr)

        print(csv_reader)
        for row in csv_reader:
            #print(row)

            setObj=ParamSet()
            if len(row)>0:
                for k,v in row.iteritems():
                    print('setting... '+str(k)+': '+str(v))
                    setObj.SetValueByAKA(k, v, dbg)
                values=[]
                objects.append(setObj)
            else:
                print('no have entries. length: '+str(len(row)))

    print('ReadCSVFile: obtained dictionaries: ', len(objects))
    #print objects
    return objects


def ReadDataFile(fileName, keyTag="NYS", splitStr=",", dbg=False):

    print('ReadDataFile: to file read ("'+splitStr+'"): '+fileName+', keyTag: '+keyTag)
    statsFile=open(fileName,'r')

    objects=[]
    keys=[]
    values=[]

    for line in statsFile:
        lineStr=str(line)
        #print(lineStr)
        if keyTag.lower() in lineStr.lower():
            keys=lineStr.split(splitStr)
        else:
            values=lineStr.split(splitStr)

        setObj=ParamSet()
        if len(keys)>0 and len(values)==len(keys):
            for k,v in zip(keys,values):
                print('setting... '+str(k)+': '+str(v))
                setObj.SetValueByAKA(k, v, dbg)
            values=[]
            objects.append(setObj)
        else:
            print('no have keys or mismatch. keys: '+str(len(keys))+', values: '+str(len(values)))

    print('ReadDataFile: obtained dictionaries: ',len(objects))
    return objects

class Param:
    name="NYS"
    akas=[]
    value=-1
    def __init__(self,n,a,v):
        self.name=n
        self.akas=a
        self.value=v
        #print('Param initialised: '+self.name)

    def PrintParam(self):
        outStr="Param... name: "+str(self.name)+", akas: "+str(self.akas)+", value: "+str(self.value)
        print(outStr)


class ParamSet():
    def __init__(self, dbg=False):
        if dbg: print('ParamSet initialised!')
        self.id = Param("modID",["modID","id"],None)
        self.fe = Param("frontEnd",["frontEnd","FE"],None)
        self.bias = Param("bias",["bias","V","voltage"],None)
        self.thla = Param("thla",["thl(adc)","threshold"],None)
        self.thle = Param("thle",["thl(e)","threshold"],None)
        self.pitch = Param("pitch",["pitch"],None)
        self.cluX = Param("cluSizeX",["cluSizeX","cluX","clusterSizeX"],None)
        self.cluY = Param("cluSizeY",["cluSizeY","cluY","clusterSizeY"],None)
        self.cluTot = Param("cluSizeTot",["cluSizeTot","cluT","cluTot","totalClusterSize"],None)
        self.mult = Param("multiplicity",["multiplicity","cluM","mult","eventMultiplicity"],None)
        self.sig = Param("signal",["signal","cluS","clusterSignal"],None)
        self.cluStat = Param("cluStat",["cluStat","clusterStatistics"],None)
        self.resX = Param("residualX",["residualX","resX"],None)
        self.resY = Param("residualY",["residualY","resY"],None)
        self.resStat = Param("resStat",["resStat","residualStatistics"],None)
        self.tb = Param("testbeam",["testbeam"],None)
        self.eff = Param("efficiency",["efficiency","eff"],None)
        self.ptb = Param("PTB",["PTB","punch-through"],None)
        self.run = Param("run",["run","runNumber","run#"],None)
        self.set = Param("set",["set","runSet"],None)

    def SetValueByName(self, key, val, dbg):
        if dbg: print('ParamSet.SetValueByName! '+key)
        found=False
        for attr, value in self.__dict__.iteritems():
            #print(value.name)
            if key.lower() in value.name.lower():
                if dbg: print('found match for '+key+": "+str(value.akas))
                value.value=val
                #value.PrintParam()
                found=True
        if found==False: print('ParamSet.SetValueByName No match found!')

    def GetValueByName(self, key, dbg):
        if dbg: print('ParamSet.GetValueByName! '+key)
        found=False
        for attr, value in self.__dict__.iteritems():
            #print(value.akas)
            if key.lower() in value.name.lower():
                if dbg: print('found match for '+key+': '+str(value.name))
                return value.value
                found=True
        if found==False: print('ParamSet.GetValueByName No match found!')

    def SetValueByAKA(self, key, val, dbg):
        if dbg: print('ParamSet.SetValueByAKA! '+key)
        found=False
        for attr, value in self.__dict__.iteritems():
            #print(value.akas)
            if key.lower() in [a.lower() for a in value.akas]:
                if dbg: print('found match for '+key+": "+str(value.akas))
                value.value=val
                value.PrintParam()
                found=True
        if found==False: print('ParamSet.SetValueByAKA No match found!')

    def GetValueByAKA(self, key, dbg):
        if dbg: print('ParamSet.GetValueByAKA! '+key)
        found=False
        for attr, value in self.__dict__.iteritems():
            #print(value.akas)
            if key.lower() in [a.lower() for a in value.akas]:
                if dbg: print('found match for '+key+": "+str(value.akas))
                return value.value
                found=True
        if found==False: print('ParamSet.GetValueByAKA No match found!')

    def PrintSet(self, dbg):
        if dbg: print('ParamSet.PrintSet!')
        for attr, value in self.__dict__.iteritems():
            if value.value!=None:
                value.PrintParam()

    def GetSetInfo(self, dbg):
        if dbg: print('ParamSet.GetSetInfo!')
        infoDict={}
        for attr, value in self.__dict__.iteritems():
            if value.value!=None:
                try:
                    infoDict[value.name]=int(value.value)
                except:
                    try:
                        infoDict[value.name]=float(value.value)
                    except:
                        infoDict[value.name]=value.value
        return infoDict

    def GetSetKeys(self):
        return [str(value.name) for attr, value in self.__dict__.iteritems()]

    def GetSetValues(self):
        return [str(value.value) for attr, value in self.__dict__.iteritems()]

    def GetSetAKAs(self):
        akasArr=[]
        [ akasArr.extend([str(aka) for aka in value.akas]) for attr, value in self.__dict__.iteritems()]
        return akasArr

    def MakeDict(self):
        dict={}
        for attr, value in self.__dict__.iteritems():
            try:
                dict[str(value.name)]=int(value.value)
            except:
                try:
                    dict[str(value.name)]=float(value.value)
                except:
                    dict[str(value.name)]=str(value.value)

        return dict
