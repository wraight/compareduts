### code to enter dut data

import commonCode as cc
import time
import argparse
import glob
import json
import os
import sys
sys.path.insert(0, '/Users/kwraight/repositories/databases/')
import CommonCode as dbc


######################
### parsing
######################

def GetArgs():

    parser = argparse.ArgumentParser(description=__file__)

    parser.add_argument('--dir', help='name of stat directory (default no set)')
    parser.add_argument('--sumFile', help='specify summary file (default no set)')
    parser.add_argument('--test', help='use test settings')
    parser.add_argument('--debug', help='debug flag')
    parser.add_argument('--col', help='collection name for upload')
    parser.add_argument('--db', help='database name for upload')
    parser.add_argument('--mode', help='mode for upload')

    args = parser.parse_args()

    print('args: '+str(args))

    argDict={'dir':"NYS", 'sumFile':"NYS", 'test':False, 'debug':False, 'outFile':"testFile.txt", 'db':"Jeremy", 'col':"Bearimy", 'mode':"replace"}

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

######################
### main
######################

def main():

    argDict=GetArgs()
    print('argDict: '+str(argDict))

    if argDict['test']:
        print "using test values:\n"+str(argDict)
        testFile= open(argDict['outFile'],'w')
        testFile.write("testFile for: "+str(argDict)+"\n\n")


    if "NYS" in argDict['dir']:
        print('no data directory set. exiting')
        return

    ## look for directory
    try:
        os.chdir(argDict['dir'])
    except OSError:
        print('no directory found matching '+argDict['dir']+'. exiting')
        return

    ## look for summary file
    summaryFile=argDict['dir']+"/"+argDict['sumFile']
    summaryFile=summaryFile.replace("//","/")
    if "NYS" in summaryFile:
        print('no summary set in '+argDict['sumFile'])
        for file in glob.glob(argDict['dir']+'*Summary*.csv'):
            print(file)
            summaryFile=file

    if "NYS" in summaryFile:
        print('no summary found in '+argDict['dir']+'. exiting')
        return

    ## get directory info
    sumDicts=cc.ReadDataFile(summaryFile,"DUT", ",", argDict['debug'])
    print('DUT files found: '+str(len(sumDicts)))


    collection=dbc.SetCollection(argDict['db'], argDict['col'], argDict['mode'])

    # get DUT files
    for dutDict in sumDicts:
        dutDict.PrintSet(argDict['debug'])
        #print(dutDict.GetValueByAKA("id", argDict['debug']))
        dutFile=summaryFile[:summaryFile.rfind("_")+1]+dutDict.GetValueByAKA("id", argDict['debug'])+".csv"
        print('DUT file search: '+dutFile)
        runDicts=cc.ReadDataFile(dutFile,"Run", ",", argDict['debug'])
        print('DUT file found: '+dutFile)

        print('Runs found: '+str(len(runDicts)))

        dutInfo=dutDict.GetSetInfo(argDict['debug'])
        for r in runDicts:
            r.PrintSet(argDict['debug'])
            runInfo=r.GetSetInfo(argDict['debug'])
            fullInfo = dict(dutInfo)  # or orig.copy()
            fullInfo.update(runInfo)
            if argDict['test']:
                content=", ".join(list(fullInfo.keys()))+"\n"
                content+=", ".join([str(v) for v in list(fullInfo.values())])+"\n\n"
                #testFile.write(content)
                print('output:\n'+content)
            else:
                try:
                    result=collection.insert_one(fullInfo)
                except AttributeError:
                    print('!!! Upload failed. Are you sure collection exists?')
                    break
                except:
                    print('!!! Problem with upload')
                    break


    if argDict['test']:
        testFile.close()

    print('all done!')
    return



if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"
