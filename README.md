# Compare DUT parameters

### Repository for python based analysis code to compare measured DUT parameters

Required python libraries:
* time
* argparse
* json
* glob
* matplotlib.pyplot

---

# Basic Schema
Read formatted *csv* files from specified directory then (some/all):
* Upload to database
* Output file
* Plot

Two formatted files required:
1. Summary file: *XXXSummary.csv*
  * Per module: DUT, ID, pitch, PTB, bias, THL, testbeam
  * XXX is testbeam ID string, e.g. *DESY2018Dec*
2. DUT file(s): *XXX_YYY.csv*
  * Per run: Run, Efficiency, ClusterSizeX, ClusterSizeY, ResidualX, ResidualY
  * YYY is DUT ID, e.g. *3292-1-45*, this should be found in Summary file

Upload to [MongoDB](https://www.mongodb.com) database
* **NB** uses https://github.com/kwraight/databases code

---

# Example Recipe
1. Make summary *json* file with per run information
  > python gleanTB.py --dir PATH_TO_DATA/DESY2019July/ --sumFile DESY2019July_Summary.csv --outFile DESY2019July_DB

2. Upload summary *json* file to database
  > python PATH_TO_DB_CODE/sendJSON.py --mode create --database testbeam --collection Desy2019July --file DESY2019July_DB.json --name duts

---

## read testbeam information and upload to database
Read formatted *csv* files from specified directory then upload to database

**Command**

`readDUTs.py`

| Arg | Description (default) | e.g. |
| --- | --- | --- |
| dir | name of stat directory (default no set) | /PATH_TO_DIR/DESY2018Dec/ |
| sumFile | optional: specify summary file (default no set) | DESY2018Dec_Summary |
| test | no upload and output file (default False) | 1 |
| debug | increase output (default False) | 1 |
| db | database name for upload (Jeremy) | |
| col | collection name for upload (Bearimy) | |
| mode | mode for upload: create/update/replace/delete (update) | |

*E.g.*

`python readDUTs.py --dir /Users/kwraight/ATLAS_testbeam/DESY2018Dec/ --debug 0 --db Jeremy --col Bearimy --mode replace`

**Comment**

By default database will replace existing information

## read testbeam information and create json file
Read formatted *csv* files from specified directory then make JSON file

**Command**

`gleanTB.py`

| Arg | Description (default) | e.g. |
| --- | --- | --- |
| dir | name of stat directory (default no set) | /PATH_TO_DIR/DESY2018Dec/ |
| sumFile | specify input summary file name (default no set) | DESY2018Dec_Summary |
| outFile | specify output json file name (summaryFile.json) | DESY2018Dec_DB |
| debug | increase output (default False) | 1 |

*E.g.*

`python gleanTB.py --dir PATH_TO_DATA/DESY2019July/ --sumFile DESY2019July_Summary.csv --outFile DESY2019July_DB`

**Comment**


## download information from database
**_to do_**

## plot information
**_to do_**
