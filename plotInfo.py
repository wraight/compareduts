### code to enter dut data

import commonCode as cc
import time
import argparse
import glob
import json
import os
import sys
sys.path.insert(0, '/Users/kwraight/repositories/databases/')
import CommonCode as dbc


######################
### parsing
######################

def GetArgs():

    parser = argparse.ArgumentParser(description=__file__)

    parser.add_argument('--dataFile', help='specify data file (default no set)')
    parser.add_argument('--debug', help='debug flag')
    parser.add_argument('--path', help='path to stat files (default "/Users/kwraight/CERN_repositories/streamsim/testDir/")')
    parser.add_argument('--name', help='name (default not set)')
    parser.add_argument('--xs', help='variable name for X axis (default: run#)')
    parser.add_argument('--ys', help='vaiable name for Y axis, multiple possible (default: run#)')
    args = parser.parse_args()

    print('args: '+str(args))

    argDict={'dir':"NYS", 'dataFile':"NYS", 'debug':False, 'outFile':"NYS", 'path':"NYS", 'xs':"run", 'ys':["run"] }

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

######################
### useful functions
######################

def GetFiles(path,ext=".json"):

    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if ext in file:
                files.append(os.path.join(r, file))
    print "Found",len(files),"in",path

    return files

def GetJSON(fileName, name='DUT'):

    statArr=[]
    names=[]
    vals=[]

    with open(fileName) as json_file:
        data = json.load(json_file)
        try:
            for p in data[name]:
                statArr.append(p)
        except KeyError:
            print('no key found matching: '+name)
        except:
            print('problem reading file')

    return statArr

######################
### main
######################

def main():

    argDict=GetArgs()
    print('argDict: '+str(argDict))

### a)
    myFiles=[]
    if "NYS" in argDict['dataFile']:
        myFiles= GetFiles(argDict['path'], argDict['ext'])
    else:
        myFiles.append(argDict['dataFile'])
    print('files found: '+str(len(myFiles)))

    ### b)
    count=0
    for f in myFiles:
        print('working on: '+f)
        #fileInfo= GetJSON(f, argDict['name'])
        with open(f) as dataFile:
            fileInfo = json.load(dataFile)
        print(fileInfo)

        #Step 4: Insert object directly into MongoDB via insert_one
        retArr=GetJSON(f,argDict['name'])
        if len(retArr)<0:
            print('no data returned from file: maybe check name')
            continue


    '''
    TO DO: filter data, plot
    '''

    print('all done!')
    return



if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"
