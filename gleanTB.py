### code to enter dut data

import commonCode as cc
import time
import argparse
import glob
import json
import os
import sys


######################
### parsing
######################

def GetArgs():

    parser = argparse.ArgumentParser(description=__file__)

    parser.add_argument('--dir', help='name of stat directory (default no set)')
    parser.add_argument('--outFile', help='specify output json file name (default no set)')
    parser.add_argument('--sumFile', help='specify input summary file name (default no set)')
    parser.add_argument('--debug', help='debug flag')

    args = parser.parse_args()

    print('args: '+str(args))

    argDict={'dir':"NYS", 'outFile':"summaryFile", 'sumFile':"NYS", 'debug':False }

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                try:
                    argDict[a[0]]=float(a[1])
                except:
                    argDict[a[0]]=a[1]

    return argDict

######################
### main
######################

def main():

    argDict=GetArgs()
    print('argDict: '+str(argDict))

    thisDir=os.getcwd()

    if "NYS" in argDict['dir']:
        print('no data directory set. exiting')
        return

    ## look for directory
    try:
        os.chdir(argDict['dir'])
    except OSError:
        print('no directory found matching '+argDict['dir']+'. exiting')
        return

    ## look for summary file
    summaryFile=argDict['dir']+"/"+argDict['sumFile']
    summaryFile=summaryFile.replace("//","/")
    if "NYS" in summaryFile:
        print('no summary set in '+argDict['sumFile'])
        for file in glob.glob(argDict['dir']+'*Summary*.csv'):
            print(file)
            summaryFile=file

    if "NYS" in summaryFile:
        print('no summary found in '+argDict['dir']+'. exiting')
        return

    ## get directory info
    sumDicts=cc.ReadCSVFile(summaryFile, ",", argDict['debug'])
    print('DUT files found: '+str(len(sumDicts)))


    data={}
    data['duts']=[]
    # get DUT files
    for dutDict in sumDicts:
        dutDict.PrintSet(argDict['debug'])
        print('was said')

        #print(dutDict.GetValueByAKA("id", argDict['debug']))
        print(summaryFile[:summaryFile.rfind("_")+1])
        print(dutDict.GetValueByAKA("id", argDict['debug']))
        print(dutDict.GetValueByAKA("set", argDict['debug']))
        dutFile=summaryFile[:summaryFile.rfind("_")+1]+dutDict.GetValueByAKA("id", argDict['debug'])+"_"+dutDict.GetValueByAKA("set", argDict['debug'])+".csv"
        print('DUT file search: '+dutFile)
        runDicts=cc.ReadCSVFile(dutFile, ",", argDict['debug'])
        print('DUT file found: '+dutFile)

        print('Runs found: '+str(len(runDicts)))

        dutInfo=dutDict.GetSetInfo(argDict['debug'])
        dutInfo['runs']=[]

        for r in runDicts:
            r.PrintSet(argDict['debug'])
            runInfo=r.GetSetInfo(argDict['debug'])
            dutInfo['runs'].append(runInfo)

        data['duts'].append(dutInfo)
        print('length: '+str(len(data['duts'])))

    saveFile=argDict['outFile']
    if "json" not in saveFile[-4:]:
        saveFile+=".json"
    with open(thisDir+'/'+saveFile, 'w') as outfile:
        print('printing to json file')
        json.dump(data, outfile)

    print('all done!')
    return



if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"
