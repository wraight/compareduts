### code to enter dut data

import commonCode as cc
import time
import argparse
import numpy as np
import json


######################
### parsing
######################

def GetArgs():

    parser = argparse.ArgumentParser(description=__file__)

    parser.add_argument('--file', help='name of save file')
    parser.add_argument('--set', nargs='+', help='filter files')
    parser.add_argument('--test', help='use test settings')

    args = parser.parse_args()

    print "args:",args

    argDict={'set':[], 'file':"NYS", 'test':False, 'debug':False }

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

def TestSet(argDict):

    testDict={'set':["id:testID", "pitch:50x50", "thl:"+str(int(np.random.uniform(1, 1000, 1)[0])), "bias:"+str(int(np.random.uniform(100, 200, 1)[0])), "cluX:"+str(np.random.uniform(1.0, 2.0, 1)[0]), "cluY:"+str(np.random.uniform(1.0, 2.0, 1)[0]), "cluT:"+str(np.random.uniform(1.2, 2.2, 1)[0]), "cluS:"+str(int(np.random.uniform(0, 32, 1)[0])), "cluM:"+str(np.random.uniform(1.0, 2.0, 1)[0])], 'file':"testfile.json", 'test':True, 'debug':True }

    for t in argDict.keys():
        argDict[t]=testDict[t]

    return

######################
### main
######################

def main():

    argDict=GetArgs()
    print "argDict:",argDict

    if argDict['test']:
        TestSet(argDict)
        print "using test values:\n"+str(argDict)

    setObj=cc.DUTSet()

    for s in argDict['set']:
        if ":" not in s:
            print "skipping set:",s
            continue
        setObj.SetValueByAKA(s.split(':')[0], s.split(':')[1])

    inStr=raw_input("write to file: "+argDict['file']+" (y/n)\t")

    if not "y" in inStr.lower():
        print "*** Acht! Then why bother?! ***"
    else:
        if "json" not in argDict['file'][-4:].lower():
            argDict['file']+=".json"
        with open(argDict['file'], 'w') as outfile:
            data = {}
            data['DUT'] = []
            data['DUT'].append(setObj.MakeDict())
            json.dump(data, outfile)

    with open(argDict['file']) as json_file:
        data = json.load(json_file)
        for p in data['people']:
            print('HERE IS SOME INFO...')
            for a in vars(p).iteritems():
                print "a[0]]

#for o in readObjs: o.PrintSet()

    return



if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"
