### code to plot dut data

import commonCode as cc
import time
import argparse
import numpy as np
import matplotlib.pyplot as plt



######################
### parsing
######################

def GetArgs():

    parser = argparse.ArgumentParser(description=__file__)


    parser.add_argument('--file', help='name of save file')
    parser.add_argument('--xs', help='variable for x axis')
    parser.add_argument('--ys', help='variable for y axis')
    parser.add_argument('--snips', nargs='+', help='filter files')
    parser.add_argument('--test', help='use test settings')

    args = parser.parse_args()

    print('args: '+str(args))

    argDict={'file':"NYS", 'test':False, 'debug':False  }

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

def TestSet(argDict):

    testDict={ 'file':"testfile.txt", 'test':True, 'debug':True }

    for t in argDict.keys():
        argDict[t]=testDict[t]

    return

def PlotVal(plotDicts):

    styles=["-","--","-.",":"]
    count=0
    plt.figure()
    for p in plotDicts:
        count+=1
        plt.plot( p['xs'], p['ys'], linestyle=styles[count], label=p['name'])
    plt.title("myPlot")
    plt.ylabel("Ys")
    plt.xlabel("Xs")
    plt.grid(True)
    plt.legend(loc="upper right")

    plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.95, hspace=0.40, wspace=0.409) # plots layout
    plt.show()

    return

######################
### main
######################

def main():

    argDict=GetArgs()
    print "argDict:",argDict

    fileSets=cc.ReadStatsFile(argDict['file'])
    print "read object sets:",len(fileSets)

    plotDicts=[]

    for s in argDict['snips']:
        if ":" not in s:
            print "skipping snip:",s
            continue
        if s.split(':')[0] in [p['name'] for p in plotDicts]:
            print "already have:",s.split(':')[0]
            for p in plotDicts:
                if s.split(':')[0] in p['name']:
                    p['filters'].append(s.split(':')[1])
                    break
            continue
        plotDicts.append({'name':s.split(':')[0], 'filters':[s.split(':')[1]], 'xs':[], 'ys':[]})

    print plotDicts

    for f in fileSets:
        #print "fileSet:",f.PrintSet()
        for p in plotDicts:
            if len(p['filters'])<1:
                print "no filters! skipping"
                continue
            found=True
            for x in p['filters']:
                if x not in f.id.value:
                    found=False
                    break
            print "result for:",p['filters']
            if found:
                print "got one!"
                if argDict['xs'] not in f.GetSetAKAs():
                    print "no key found for:",argDict['xs']
                else:
                    p['xs'].append(f.GetValueByAKA(argDict['xs']))
                if argDict['ys'] not in f.GetSetAKAs():
                    print "no key found for:",argDict['ys']
                else:
                    p['ys'].append(f.GetValueByAKA(argDict['ys']))

    for p in plotDicts:
        print p

    '''
        order entries by x array
        '''

    PlotVal(plotDicts)


    return



if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"
